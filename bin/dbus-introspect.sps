#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

(import
  (rnrs)
  (d-bus protocol connections)
  (d-bus protocol messages))

(define bus (d-bus-connect))

(define destination
  (if (null? (cdr (command-line)))
      "org.freedesktop.DBus"
      (cadr (command-line))))

(define path
  (if (< (length (command-line)) 3)
      "/"
      (caddr (command-line))))

(d-bus-write-message bus
                     (make-d-bus-message
                      MESSAGE_TYPE_METHOD_CALL 0 #f '()
                      `#(,(header-PATH        "/org/freedesktop/DBus")
                         ,(header-DESTINATION "org.freedesktop.DBus")
                         ,(header-INTERFACE   "org.freedesktop.DBus")
                         ,(header-MEMBER      "Hello"))
                      #f))

(let ((call-serial
       (d-bus-write-message bus
                            (make-d-bus-message
                              MESSAGE_TYPE_METHOD_CALL 0 #f '()
                              `#(,(header-PATH        path)
                                 ,(header-DESTINATION destination)
                                 ,(header-INTERFACE   "org.freedesktop.DBus.Introspectable")
                                 ,(header-MEMBER      "Introspect"))
                              #f))))
  (d-bus-conn-flush bus)
  (let lp ()
    (let ((msg (d-bus-read-message bus)))
      (let ((return-serial (d-bus-headers-ref (d-bus-message-headers msg) 'REPLY_SERIAL))
            (type (d-bus-message-type msg)))
        (cond
          ((not (and (eqv? call-serial return-serial)
                     (or (eqv? type MESSAGE_TYPE_METHOD_RETURN)
                         (eqv? type MESSAGE_TYPE_ERROR))))
           (lp))                        ;ignore

          ((eqv? (d-bus-message-type msg) MESSAGE_TYPE_ERROR)
           (raise (condition
                   (make-who-condition "dbus-introspect")
                   (make-irritants-condition (list msg)))))
          (else
           (display (car (d-bus-message-body msg)))
           (flush-output-port (current-output-port))))))))

(d-bus-disconnect bus)
