;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2020 Göran Weinholt <goran@weinholt.se>
;; SPDX-License-Identifier: MIT
#!r6rs

;;; Parser and formatter for D-Bus type signatures

;; The parser and the formatter should enforce constraints on the
;; types, such as how DICT_ENTRY can only appear in an ARRAY. When
;; given valid arguments: parse(format(x))==x, format(parse(y))==y.

(library (d-bus protocol signatures)
  (export
    parse-type-signature
    format-type-signature)
  (import
    (rnrs)
    (d-bus compat match)
    (packrat))

;; Wizardry
(define signature-parser
  (packrat-parser
   expr
   (expr  [(a <- block '#f)                `(message . ,a)])
   (block [(a <- type b <- block)          `(,a . ,b)]
          [(a <- type)                     `(,a)])
   (type  [(a <- basic)                     a]
          [('#\v)                           'VARIANT]
          [('#\( a <- block '#\))           `(STRUCT ,@a)]
          [('#\a a <- atype)                `(ARRAY ,a)])
   (atype [('#\{ k <- basic v <- type '#\}) `(DICT_ENTRY ,k ,v)]
          [(a <- type)                      a])
   (basic [('#\y)                           'BYTE]
          [('#\b)                           'BOOLEAN]
          [('#\n)                           'INT16]
          [('#\q)                           'UINT16]
          [('#\i)                           'INT32]
          [('#\u)                           'UINT32]
          [('#\x)                           'INT64]
          [('#\t)                           'UINT64]
          [('#\d)                           'DOUBLE]
          [('#\s)                           'STRING]
          [('#\o)                           'OBJECT_PATH]
          [('#\g)                           'SIGNATURE]
          [('#\h)                           'UNIX_FD])))

(define (parse-type-signature x)
  (let ((g (packrat-string-results #f x)))
    (let ((result (signature-parser g)))
      (if (parse-result-successful? result)
          (parse-result-semantic-value result)
          (let ((e (parse-result-error result)))
            (assertion-violation 'parse-type-signature
                                 "Invalid type signature" x
                                 (parse-position-column (parse-error-position e))
                                 (parse-error-expected e)
                                 (parse-error-messages e)))))))

;; Reverse wizardry
(define (format-type-signature sig)
  (define (fail x)
    (assertion-violation 'format-type-signature "Invalid signature" sig x))
  (call-with-string-output-port
    (lambda (p)
      (match sig
        [('message type* ...)
         (let lp ((type* type*) (in-array? #f) (only-basic? #f))
           (for-each
            (match-lambda
             [(and ('STRUCT . type*) x)
              (when only-basic? (fail x))
              (put-char p #\()
              (lp type* #f #f)
              (put-char p #\))]
             [(and ('ARRAY . atype*) x)
              (when only-basic? (fail x))
              (unless (and (pair? atype*) (null? (cdr atype*))) (fail x))
              (put-char p #\a)
              (lp atype* #t #f)]
             [(and ('DICT_ENTRY a b) x)
              (when only-basic? (fail x))
              (unless in-array? (fail x))
              (put-char p #\{)
              (lp (list a b) #f #t)
              (put-char p #\})]
             [(and 'VARIANT x)
              (when only-basic? (fail x))
              (put-char p #\v)]
             ['BYTE        (put-char p #\y)]
             ['BOOLEAN     (put-char p #\b)]
             ['INT16       (put-char p #\n)]
             ['UINT16      (put-char p #\q)]
             ['INT32       (put-char p #\i)]
             ['UINT32      (put-char p #\u)]
             ['INT64       (put-char p #\x)]
             ['UINT64      (put-char p #\t)]
             ['DOUBLE      (put-char p #\d)]
             ['STRING      (put-char p #\s)]
             ['OBJECT_PATH (put-char p #\o)]
             ['SIGNATURE   (put-char p #\g)]
             ['UNIX_FD     (put-char p #\h)]
             [x (fail x)])
            type*))]
        [x (fail x)])))))
