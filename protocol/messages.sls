;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; D-Bus messages

(library (d-bus protocol messages)
  (export
    make-d-bus-message d-bus-message?
    d-bus-message-type
    d-bus-message-flags
    d-bus-message-serial
    d-bus-message-fds
    d-bus-message-headers
    d-bus-message-body

    MESSAGE_TYPE_METHOD_CALL
    MESSAGE_TYPE_METHOD_RETURN
    MESSAGE_TYPE_ERROR
    MESSAGE_TYPE_SIGNAL

    FLAG_NO_REPLY_EXPECTED
    FLAG_NO_AUTO_START
    FLAG_ALLOW_INTERACTIVE_AUTHORIZATION

    header-PATH
    header-INTERFACE
    header-MEMBER
    header-ERROR_NAME
    header-REPLY_SERIAL
    header-DESTINATION
    header-SENDER
    header-SIGNATURE
    header-UNIX_FDS

    d-bus-headers-ref
    d-bus-headers->alist

    d-bus-write-message
    d-bus-read-message)
  (import
    (rnrs)
    (d-bus compat socket)
    (d-bus protocol connections)
    (d-bus protocol signatures)
    (d-bus protocol utils)
    (d-bus protocol wire)
    #;
    (industria hexdump))

(define (log-hex bv len prefix)
  #f
  #;
  (hexdump #f bv 0 len prefix))

(define-record-type d-bus-message
  (sealed #t)
  (fields type
          flags
          serial
          fds
          headers
          body))

(define MESSAGE_TYPE_INVALID       0)
(define MESSAGE_TYPE_METHOD_CALL   1)
(define MESSAGE_TYPE_METHOD_RETURN 2)
(define MESSAGE_TYPE_ERROR         3)
(define MESSAGE_TYPE_SIGNAL        4)

(define FLAG_NO_REPLY_EXPECTED               #x1)
(define FLAG_NO_AUTO_START                   #x2)
(define FLAG_ALLOW_INTERACTIVE_AUTHORIZATION #x4)

(define PROTOCOL_VERSION 1)

(define header-field-names
  '#(INVALID PATH INTERFACE MEMBER ERROR_NAME REPLY_SERIAL
     DESTINATION SENDER SIGNATURE UNIX_FDS))

(define (header-PATH x)          `(1 (OBJECT_PATH . ,x)))
(define (header-INTERFACE x)     `(2 (STRING . ,x)))
(define (header-MEMBER x)        `(3 (STRING . ,x)))
(define (header-ERROR_NAME x)    `(4 (STRING . ,x)))
(define (header-REPLY_SERIAL x)  `(5 (UINT32 . ,x)))
(define (header-DESTINATION x)   `(6 (STRING . ,x)))
(define (header-SENDER x)        `(7 (STRING . ,x)))
(define (header-SIGNATURE x)     `(8 (SIGNATURE . ,x)))   ;XXX: can be ""
(define (header-UNIX_FDS x)      `(9 (UINT32 . ,x)))

(define d-bus-header-signature
  '(message BYTE BYTE BYTE BYTE UINT32 UINT32 (ARRAY (STRUCT BYTE VARIANT))))

(define (d-bus-headers-ref headers key)
  (let lp ((i 0))
    (if (eqv? i (vector-length headers))
        #f
        (let* ((header (vector-ref headers i))
               (hkey-id (car header))
               (hkey (and (< hkey-id (vector-length header-field-names))
                          (vector-ref header-field-names hkey-id))))
          (if (eq? hkey key)
              (cdadr header)
              (lp (fx+ i 1)))))))

(define (d-bus-headers->alist headers)
  (let lp ((i 0) (ret '()))
    (if (eqv? i (vector-length headers))
        (reverse ret)
        (let* ((header (vector-ref headers i))
               (hkey-id (car header)))
          (cond ((and (< hkey-id (vector-length header-field-names))
                      (vector-ref header-field-names hkey-id))
                 => (lambda (key)
                      (lp (fx+ i 1)
                          (cons (cons key (cdadr header)) ret))))
                (else
                 (lp (fx+ i 1) ret)))))))

;; Write a message on the connection. Might need d-bus-flush
;; afterwards. Returns the serial number of the written mesage.
(define (d-bus-write-message conn msg)
  (let ((endian (if (eq? (native-endianness) (endianness little))
                    (char->integer #\l)
                    (char->integer #\B)))
        (serial (or (d-bus-message-serial msg)
                    (d-bus-conn-next-serial! conn))))
    (let*-values ([(body bodylen)
                   (cond
                     ((d-bus-message-body msg) =>
                      (lambda (body)
                        (let* ((buf (make-d-bus-buffer))
                               (sig (or (d-bus-headers-ref (d-bus-message-headers msg)
                                                           'SIGNATURE)
                                        (assertion-violation 'd-bus-write-message
                                                             "Missing SIGNATURE header"
                                                             conn msg)))
                               (len (d-bus-marshal-message! buf 0 (parse-type-signature sig) body)))
                          (values (d-bus-buffer-data buf) len))))
                     (else (values #vu8() 0)))]
                  [(hdr hdrlen)
                   (let* ((buf (make-d-bus-buffer))
                          (len (d-bus-marshal-message! buf 0 d-bus-header-signature
                                                       `(,endian
                                                         ,(d-bus-message-type msg)
                                                         ,(d-bus-message-flags msg)
                                                         ,PROTOCOL_VERSION
                                                         ,bodylen
                                                         ,serial
                                                         ,(d-bus-message-headers msg)))))
                     (values (d-bus-buffer-data buf) (fxalign len 8)))])
      (log-hex hdr hdrlen "→ ")
      (log-hex body bodylen "→ ")
      (socket-sendmsg (d-bus-conn-socket conn)
                      hdr hdrlen
                      body bodylen
                      (d-bus-message-fds msg)))
    serial))

(define d-bus-read-message
  (case-lambda
    ((conn)
     (d-bus-read-message conn #f))
    ((conn accept-fds)
     (define who 'd-bus-read-message)
     (define hdrlen 16)              ;fixed and known part of the headers
     (define get-fds (or (d-bus-conn-fd-dequeue conn) (lambda () '())))
     (define (message-endianness bv)
       (case (integer->char (bytevector-u8-ref bv 0))
         [(#\l) (endianness little)]
         [(#\B) (endianness big)]
         [else (error who "Invalid message" bv)]))
     (let ((hdr (get-bytevector-n (d-bus-conn-inport conn) hdrlen)))
       (unless (and (bytevector? hdr) (eqv? (bytevector-length hdr) hdrlen))
         (error who "Short read" hdr))
       (unless (eqv? (bytevector-u8-ref hdr 3) PROTOCOL_VERSION)
         (error who "Invalid protocol version" hdr))
       (let* ((endian (message-endianness hdr))
              (body-len (bytevector-u32-ref hdr 4 endian))
              (headers-len (bytevector-u32-ref hdr 12 endian))
              (msg-len (+ body-len (fxalign (fx+ hdrlen headers-len) 8))))
         (when (> headers-len (expt 2 26))
           (error who "Overlong header array" hdr))
         ;; XXX: what is the limit on the body?
         (let ((bv (make-bytevector msg-len)))
           (bytevector-copy! hdr 0 bv 0 hdrlen)
           (let* ((n (get-bytevector-n! (d-bus-conn-inport conn) bv hdrlen (- msg-len hdrlen)))
                  (fds (get-fds)))
             (when (not accept-fds)
               (for-each close-fdes fds))
             (unless (= n (- msg-len hdrlen))
               (error who "Short message" hdr n))
             (log-hex bv (bytevector-length bv) "← ")
             (let*-values ([(offset full-header)
                            (d-bus-unmarshal-message bv 0 endian d-bus-header-signature)]
                           [(_endian message-type flags _version body-length serial headers)
                            (apply values full-header)]
                           [(offset body)
                            (let ((sig (d-bus-headers-ref headers 'SIGNATURE)))
                              (cond ((and sig (not (equal? sig "")))
                                     (let ((type (parse-type-signature sig)))
                                       (d-bus-unmarshal-message bv (fxalign offset 8)
                                                                endian type)))
                                    (else (values (fxalign offset 8) #f))))])
               (let ((trailing (- (bytevector-length bv) offset)))
                 (unless (eqv? trailing 0)
                   (error who "Trailing bytes after the message" trailing bv))
                 (make-d-bus-message message-type flags serial fds headers
                                     body)))))))))))
