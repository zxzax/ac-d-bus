#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

(import
  (rnrs)
  (d-bus protocol connections)
  (d-bus protocol messages))

(display "Connecting\n")

(define bus (d-bus-connect))

(d-bus-write-message bus
                     (make-d-bus-message MESSAGE_TYPE_METHOD_CALL 0 #f '()
                                         `#(,(header-PATH        "/org/freedesktop/DBus")
                                            ,(header-DESTINATION "org.freedesktop.DBus")
                                            ,(header-INTERFACE   "org.freedesktop.DBus")
                                            ,(header-MEMBER      "Hello"))
                                         #f))

(d-bus-conn-flush bus)
(write (d-bus-read-message bus)) (newline)

(d-bus-write-message bus (make-d-bus-message MESSAGE_TYPE_METHOD_CALL 0 #f '()
                                             `#(,(header-PATH        "/org/freedesktop/DBus")
                                                ,(header-DESTINATION "org.freedesktop.DBus")
                                                ,(header-INTERFACE   "org.freedesktop.DBus")
                                                ,(header-MEMBER      "ListNames"))
                                             #f))

(d-bus-conn-flush bus)
(write (d-bus-read-message bus))
(newline)

(d-bus-disconnect bus)
