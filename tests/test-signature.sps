#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

(import
  (rnrs)
  (srfi :64)
  (d-bus protocol signatures))

(test-begin "parse-type-signature")

(test-equal '(message (ARRAY (DICT_ENTRY INT32 INT32)))
            (parse-type-signature "a{ii}"))

(test-equal '(message BYTE BYTE BYTE BYTE UINT32 UINT32 (ARRAY (STRUCT BYTE VARIANT)))
            (parse-type-signature "yyyyuua(yv)"))

(test-equal '(message (STRUCT INT32 (STRUCT INT32 INT32)))
            (parse-type-signature "(i(ii))"))

(test-equal '(message (STRUCT (STRUCT INT32 INT32) INT32))
            (parse-type-signature "((ii)i)"))

(test-equal '(message (ARRAY (ARRAY INT32)))
            (parse-type-signature "aai"))

(test-end)

(test-begin "format-type-signature")

(test-equal "a{ii}i"
            (format-type-signature '(message (ARRAY (DICT_ENTRY INT32 INT32)) INT32)))

(test-equal "yyyyuua(yv)"
            (format-type-signature '(message BYTE BYTE BYTE BYTE UINT32 UINT32
                                             (ARRAY (STRUCT BYTE VARIANT)))))

(test-end)

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
