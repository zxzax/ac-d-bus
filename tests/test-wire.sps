#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

(import
  (rnrs)
  (srfi :64)
  (d-bus protocol wire)

  (d-bus protocol utils)
  (d-bus protocol signatures)
  (only (d-bus protocol messages) d-bus-headers-ref)

  #;
  (industria hexdump))

(define (d-bus-marshal-message type data*)
  (call-with-bytevector-output-port
    (lambda (p) (d-bus-put-message p 0 1 type data*))))

(define (parse-message bv)
  (define header-sig
    '(message BYTE BYTE BYTE BYTE UINT32 UINT32 (ARRAY (STRUCT BYTE VARIANT))))
  (let*-values ([(offset hdr)
                 (d-bus-unmarshal-message bv 0 (endianness little) header-sig)]
                [(_endian message-type flags _version body-length serial headers)
                 (apply values hdr)])
    (let ((sig (d-bus-headers-ref headers 'SIGNATURE)))
      ;; (write (parse-type-signature sig)) (newline)
      (let-values ([(end body)
                    (if (and sig (not (equal? sig "")))
                        (d-bus-unmarshal-message bv (fxalign offset 8) (endianness little)
                                                 (parse-type-signature sig))
                        (values (fxalign offset 8) #f))])
        (list (- (bytevector-length bv) end)
              hdr body)))))

(define (roundtrip type data*)
  (let ((bv (d-bus-marshal-message type data*)))
    #;
    (hexdump #f bv)
    (let-values ([(offset decoded) (d-bus-unmarshal-message bv 0 (native-endianness) type)])
      (cond ((and (equal? decoded data*) (= (bytevector-length bv) offset))
             #t)
            (else
             (display "type: ")
             (write type)
             (newline)
             (display "data: ")
             (write data*)
             (newline)
             (display "encoded: ")
             (write bv)
             (newline)
             (display "encoded length: ")
             (write (bytevector-length bv))
             (newline)
             (display "decoded: ")
             (write decoded)
             (newline)
             (display "decode offset: ")
             (write offset)
             (newline)
             #f)))))

(test-begin "marshal")

(roundtrip '(message BYTE BOOLEAN INT16 STRING (ARRAY INT32))
           '(1 #t -1 "hej" #(1 2)))

(test-assert (roundtrip '(message BYTE BOOLEAN INT16 STRING (ARRAY INT32))
                        '(1 #t -1 "hej" #(1 2))))

(test-assert (roundtrip '(message BYTE SIGNATURE)
                        '(42 "o")))

(test-assert (roundtrip '(message SIGNATURE)
                        '("o")))

(test-assert (roundtrip '(message (STRUCT BYTE VARIANT))
                        '((3 (BYTE . #xff)))))

(test-assert (roundtrip '(message BYTE VARIANT)
                        '(3 (BYTE . #xff))))

(test-assert (roundtrip '(message (ARRAY (STRUCT INT16 INT16)))
                        '(#((1 2) (2 3)))))

(test-assert (roundtrip '(message UINT16  SIGNATURE STRING)
                        '( #x42 "o" "yyyyyyyyyyyy")))

(test-assert (roundtrip '(message BYTE BOOLEAN INT16 SIGNATURE STRING (ARRAY INT32))
                        '(1 #t -1 "o" "XXXXXXXXXXXX" #(1 2))))

(test-assert (roundtrip '(message (ARRAY (DICT_ENTRY BYTE STRING)))
                        '(#((1 . "a") (2 . "b")))))

(test-assert (roundtrip '(message (STRUCT BYTE INT16) STRING (ARRAY (DICT_ENTRY BYTE BYTE)))
                        '((1 2) "wer" #((1 . 1)))))

(test-assert (roundtrip '(message INT16 SIGNATURE STRING VARIANT)
                        '(-1 "o" "XXXXXXXXX" (INT32 . 2))))

(test-assert (roundtrip '(message STRING (ARRAY (DICT_ENTRY STRING VARIANT)) (ARRAY STRING))
                        '("org.mpris.MediaPlayer2.TrackList" #() #("XXXXXX"))))

(test-assert (roundtrip '(message BYTE BYTE BYTE BYTE UINT32 UINT32 (ARRAY (STRUCT BYTE VARIANT)))
                        '(108 2 1 1 0 6 #((6 (STRING . ":1:296"))
                                          (8 (SIGNATURE . ""))
                                          (5 (UINT32 . 4))
                                          (7 (STRING . ":1.298"))))))

(test-equal '(0
              (108 4 1 1 63 3
                   #((1 (OBJECT_PATH . "/org/mpris/MediaPlayer2"))
                     (2 (STRING . "org.freedesktop.DBus.Properties"))
                     (3 (STRING . "PropertiesChanged"))
                     (8 (SIGNATURE . "sa{sv}as"))
                     (7 (STRING . ":1.281"))))
              ("org.mpris.MediaPlayer2.TrackList"
               #()
               #("Tracks")))
            (parse-message
             #vu8(108 4 1 1 63 0 0 0 3 0 0 0 135 0 0 0 1 1 111 0 23 0 0 0 47 111 114
                      103 47 109 112 114 105 115 47 77 101 100 105 97 80 108 97 121 101 114
                      50 0 2 1 115 0 31 0 0 0 111 114 103 46 102 114 101 101 100 101 115 107
                      116 111 112 46 68 66 117 115 46 80 114 111 112 101 114 116 105 101
                      115 0 3 1 115 0 17 0 0 0 80 114 111 112 101 114 116 105 101 115 67 104
                      97 110 103 101 100 0 0 0 0 0 0 0 8 1 103 0 8 115 97 123 115 118 125
                      97 115 0 0 0 7 1 115 0 6 0 0 0 58 49 46 50 56 49 0 0 32 0 0 0 111 114
                      103 46 109 112 114 105 115 46 77 101 100 105 97 80 108 97 121 101 114
                      50 46 84 114 97 99 107 76 105 115 116 0 0 0 0 0 0 0 0 0 0 0 0 11 0 0 0
                      6 0 0 0 84 114 97 99 107 115 0)))

(test-equal '(0
              (108 2 1 1 0 6
                   #((6 (STRING . ":1.296"))
                     (8 (SIGNATURE . ""))
                     (5 (UINT32 . 4))
                     (7 (STRING . ":1.298"))))
              #f)
            (parse-message
             #vu8(#x6C #x02 #x01 #x01  #x00 #x00 #x00 #x00  #x06 #x00 #x00 #x00  #x2F #x00 #x00 #x00
                  #x06 #x01 #x73 #x00  #x06 #x00 #x00 #x00  #x3A #x31 #x2E #x32  #x39 #x36 #x00 #x00
                  #x08 #x01 #x67 #x00  #x00 #x00 #x00 #x00  #x05 #x01 #x75 #x00  #x04 #x00 #x00 #x00
                  #x07 #x01 #x73 #x00  #x06 #x00 #x00 #x00  #x3A #x31 #x2E #x32  #x39 #x38 #x00 #x00)))

(test-end)

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
