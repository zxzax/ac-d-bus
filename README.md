# AC/D-Bus

AC/D-Bus is an implementation of the D-Bus wire protocol for R6RS
Scheme.

D-Bus is an interprocess communication protocol popular on GNU/Linux
systems to communicate with a variety of services. Originally designed
for desktop environments, it is now used by programs like VLC media
player, BlueZ, NetworkManager, Pulseaudio, systemd (including logind
and resolved), Polkit, gnome-keyring, and many more.

## Installation

Using the language package manager [Akku.scm](https://akkuscm.org):

```
cd my-project
akku install ac-d-bus
.akku/env
```

GNU Guix provides the guile-ac-d-bus package.

## Supported Scheme implementations

Any R6RS Scheme should be possible to support. Compatibility libraries
are needed to work with Unix sockets and for getting the current
effective user id. These have working compat libraries:

* Chez Scheme
* GNU Guile (version 2.2 with `-x .guile.sls -x .sls` or 3.0 with `--r6rs`)
* Loko scheme
* Sagittarius Scheme

## Bugs

See the issues list <https://gitlab.com/weinholt/ac-d-bus/-/issues>.
Send an email to Göran if you have a bug to report and don't want to
get a GitLab account.

## References

* [D-Bus Specification](https://dbus.freedesktop.org/doc/dbus-specification.html)
