;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; Andrew K. Wright match

(library (d-bus compat match)
  (export
    match match-lambda)
  (import
    (match)))
