;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; Get the effective user id

(library (d-bus compat uid)
  (export (rename (geteuid user-effective-uid)))
  (import (only (guile) geteuid)))
