;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2020 Göran Weinholt
#!r6rs

;;; Get the effective user id

(library (d-bus compat uid)
  (export user-effective-uid)
  (import (only (srfi :170) user-effective-uid)))
