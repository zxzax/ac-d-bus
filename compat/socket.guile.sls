;; -*- mode: scheme; coding: utf-8 -*-
;; SPDX-License-Identifier: MIT
;; Copyright © 2019-2020 Göran Weinholt
#!r6rs

;;; Local ("Unix") sockets

(library (d-bus compat socket)
  (export
    local-connect
    socket-textual-ports

    socket-input-port/fds
    socket-can-pass-fds?
    socket-sendmsg

    socket-close

    close-fdes)
  (import
    (rnrs)
    (only (guile) socket PF_UNIX AF_UNIX SOCK_STREAM
          connect send recv! make-soft-port))

;; Connect to a Unix domain socket (must support abstract sockets!)
(define (local-connect filename)
  (let ((sock (socket PF_UNIX SOCK_STREAM 0)))
    (connect sock AF_UNIX filename)
    sock))

;; Get textual ports for working with a connected socket. Must use
;; eol-style crlf!
(define (socket-textual-ports sock)
  (let-values ([(bufp extract) (open-string-output-port)])
    (define (write-char^ c)
      (cond ((eqv? c #\newline)
             (put-string bufp "\r\n"))
            (else
             (put-char bufp c))))
    (define (write-string^ s)
      (string-for-each write-char^ s))
    (define (flush-output^)
      (let ((buf (extract)))
        (send sock (string->utf8 buf))))
    (define (read-char^)
      (let ((bv (make-bytevector 1)))
        (let lp ()
          (let ((n (recv! sock bv)))
            (if (eqv? n 0)
                (eof-object)
                (let ((c (integer->char (bytevector-u8-ref bv 0))))
                  (if (eqv? c #\return)
                      (lp)
                      c)))))))
    (define (close-port^) #f)
    (define buffered-chars^ #f)
    (values (make-soft-port
             (vector #f #f #f
                     read-char^ close-port^ buffered-chars^)
             "r")
            (make-soft-port
             (vector write-char^ write-string^ flush-output^
                     #f close-port^ buffered-chars^)
             "w"))))

;; Fully sends two bytevectors and a list of file descriptors.
(define (socket-sendmsg sock bv0 size0 bv1 size1 fds)
  (assert (null? fds))
  (let ((bv (make-bytevector (+ size0 size1))))
    (bytevector-copy! bv0 0 bv 0 size0)
    (bytevector-copy! bv1 0 bv size0 size1)
    (send sock bv)))

;; Returns a binary input port for the socket and a procedure that can
;; dequeue fds (or #f if not supported) that have been read since the
;; last dequeuing
(define (socket-input-port/fds sock)
  (values sock
          #f))

(define (socket-can-pass-fds? sock)
  #f)

(define (socket-close sock)
  (close-port sock))

;; Close a received file descriptor
(define (close-fdes fd)
  #f))
