# Compatibility libraries

## (d-bus compat match)

This needs to be compatible with the Andrew W. Wright pattern matcher.
The default is (chibi match).

## (d-bus compat socket)

This connects to a Unix domain socket. Some preparations have been made
to support passing of file descriptors, but it's optional. The comments
in the implementations provided should be enough to get started.

The choice that would provide the widest support with the least effort
is likely r6rs-pffi together with a C module uses uses libc for the
socket operations.

## (d-bus compat uid)

A single procedure to get the effective user id. The default is to use
SRFI 170.
